unit drzewa;

interface

    type BST = ^node;
        node = record
        argument : LongInt;
        value : LongInt;
        counter : LongInt;
        {sum : LongInt;}
        lchild,rchild : BST
    end;

    const const_maxDirectivesDone = 1000000;
    const_maxInterval = 1000000000;
    const_functionLetter = 'f';
    const_clearDirective = 'czysc'; {first letters of those 3 must be different}
    const_sumDirective = 'suma';
    const_maxIntLength = 9;

    procedure inicjuj();
    function przypisanie(argument, value : LongInt) : LongInt;
    function suma(number, left, right : LongInt) : LongInt;
    function czysc(number : LongInt) : LongInt;
    procedure finish();

    var functions : array[1..const_maxDirectivesDone] of BST;
        functionNumber : LongInt;
        countNodes : LongInt;

implementation
    procedure inicjuj();
    {initiates variables and array}
    var i : LongInt;
    begin
        for i := 1 to length(functions) do
            functions[i] := nil;
        functionNumber := 1;
        countNodes := 0;
    end;

    function find(T : BST;argument : LongInt) : BST;
    {finds a node in given BST of the arguemnt given}
    begin
        if T <> nil then
        begin
            if T^.argument = argument then
                find := T
            else if T^.argument > argument then
                find := find(T^.lchild, argument)
            else if T^.argument < argument then
                find := find(T^.rchild, argument);
        end;
    end;

    function clear(T : BST) : LongInt;
    {does the actuall clearing of given tree}
    begin
        if T <> nil then
        begin
            dec(T^.counter);
            if T^.counter <= 0 then
            begin
                if T^.lchild <> nil then
                    clear(T^.lchild);
                if T^.rchild <> nil then
                    clear(T^.rchild);
                dispose(T);
                dec(countNodes);
            end;
        end;
        clear := countNodes;
    end;

    function czysc(number : LongInt) : LongInt;
    {clears a tree at given number. Removes a node if reference counter=1. If not, reference counter is lowered by one}
    begin
        if (number=0) or (functions[number] = nil) then
            czysc := countNodes
        
        else if (number <> 1) and (functions[number] = functions[number - 1]) then
        begin
            dec(functions[number]^.counter);
            functions[number] := nil;
            czysc := countNodes;
            exit;
        end
        
        else
        begin
            if functions[number] <> nil then
                czysc := clear(functions[number])
            else
                czysc := countNodes;
        end;
        functions[number] := nil;
    end;

    function succesor(T : BST) : BST;
    {finds a node, which is succesor of given T}
    begin
        while T^.lchild <> nil do
            T := T^.lchild;
        succesor := T;
    end;

    function magic(src : BST;arg ,va : LongInt) : BST;
    {assigns given value to an arguemnt. Src points to previous tree
    Also changes value if there allready is a node with given argument, and removes the node if given value (va) is 0}
    var temp,newNode : BST;
    begin
        if src <> nil then
        begin
            new (temp);
            inc(countNodes);
            temp^.argument := src^.argument;
            temp^.value := src^.value;
            temp^.counter := 1;

            if temp^.argument = arg then
            begin;
                if va <> 0 then {change value}
                begin
                    temp^.value := va;
                    temp^.lchild := src^.lchild;
                    temp^.rchild := src^.rchild;
                    temp^.counter := 1;

                    if src^.lchild <> nil then
                        inc(src^.lchild^.counter);
                    if src^.rchild <> nil then
                        inc(src^.rchild^.counter);
                end

                else {delete the node}
                begin
                    if src^.rchild = nil then
                    begin
                        newNode := src^.lchild;
                        dispose(temp);
                        dec(countNodes);
                        temp := newNode;
                    end

                    else
                    begin
                        newNode := succesor(src^.rchild);
                        temp^.argument := newNode^.argument;
                        temp^.value := newNode^.value;
                        temp^.lchild := src^.lchild;
                        temp^.counter := 1;
                        if src^.lchild <> nil then
                            inc(src^.lchild^.counter);
                        temp^.rchild := magic(src^.rchild,newNode^.argument, 0);
                    end;
                end;
            end

            else if temp^.argument > arg then {go left}
            begin
                temp^.rchild := src^.rchild;
                if src^.rchild <> nil then
                    inc(src^.rchild^.counter);
                if src^.lchild = nil then {insert out node}
                begin
                    new (newNode);
                    inc(countNodes);
                    newNode^.argument := arg;
                    newNode^.value := va;
                    newNode^.lchild := nil;
                    newNode^.rchild := nil;
                    newNode^.counter := 1;
                    temp^.lchild := newNode;
                end

                else
                    temp^.lchild := magic(src^.lchild,arg,va);
            end

            else if temp^.argument < arg then {go right}
            begin
                temp^.lchild := src^.lchild;
                if src^.lchild <> nil then
                    inc(src^.lchild^.counter);
                if src^.rchild = nil then {insert our node}
                begin
                    new (newNode);
                    inc(countNodes);
                    newNode^.argument := arg;
                    newNode^.value := va;
                    newNode^.lchild := nil;
                    newNode^.rchild := nil;
                    newNode^.counter := 1;
                    temp^.rchild := newNode;
                end

                else
                    temp^.rchild := magic(src^.rchild, arg, va);
            end
        end;
        magic := temp;
    end;

    function przypisanie(argument, value : LongInt) : LongInt;
    {used to give an argument it's value}
    var temp : BST;
        backtrack : LongInt;
    begin
        if (functionNumber = 1) and (value = 0) then
            przypisanie := countNodes
        
        else if (functionNumber = 1) or (functions[functionNumber - 1] = nil) then {manually input a single node}
        begin
            new (temp);
            temp^.value := value;
            temp^.argument := argument;
            temp^.lchild := nil;
            temp^.rchild := nil;
            temp^.counter := 1;
            functions[functionNumber] := temp;
            inc(countNodes);
            przypisanie := countNodes;
        end

        else if suma(functionNumber - 1, argument, argument) = value then {we don't have to do anything}
        begin
            backtrack := functionNumber - 1;
            while (backtrack > 0) and (functions[functionNumber] = functions[backtrack]) do
                dec(backtrack);
            inc(functions[backtrack]^.counter);
            functions[functionNumber] := functions[backtrack];
        end

        else
            functions[functionNumber] := magic(functions[functionNumber - 1], argument, value); {we use magic to create a new tree :)}
        inc(functionNumber);
        przypisanie := countNodes;
    end;

    function sum(T : BST;left, right : LongInt) : LongInt;
    {does the actuall summing}
    begin
        sum := 0;
        if T <> nil then
        begin
            if (T^.argument >= left) and (T^.argument <= right) then
                sum := sum + T^.value;
            if T^.argument > left then
                sum := sum + sum(T^.lchild, left, right);
            if T^.argument < right then
                sum := sum + sum(T^.rchild, left, right);
        end;
    end;

    function suma(number, left, right : LongInt) : LongInt;
    {sums values of nodes (arguemnts are greater than left, but smaller than right) in a tree of given number}
    begin
        if number = 0 then
            suma := 0
        else
            suma:=sum(functions[number], left, right);
    end;

    procedure finish();
    {used to deallocate memmory}
    var i : LongInt;
    begin
        for i := functionNumber -1 downto 1 do
            czysc(i);
    end;
end.
