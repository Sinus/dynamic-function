program funkcjeDynamiczne;

uses drzewa;


procedure error();
{this procedure is called when input is not valid}
begin
    writeln('zignorowano');
end;

function isNumber(x : string) : boolean;
{checks if a given string consists of numbers only}
var i : LongInt;
begin
    isNumber:=true;
    for i := 1 to length(x) do
    begin
        if (ord(x[i]) > ord('9')) or (ord(x[i]) < ord('0')) then
            isNumber := false;
    end;
end;

function rangeCheck(x : string; max : LongInt) : boolean;
{checks if a given string (which is a number) fits our standards}
var argument, dummy : longInt;
begin
    rangeCheck := true;

    if length(x) = 0 then
        rangeCheck := false;

    if length(x) > const_maxIntLength then
        rangeCheck := false
    else
        val(x, argument, dummy);

    if (rangeCheck = true) and (argument > max) then
        rangeCheck := false;
end;

function leadingZeros(x : string) : boolean;
{checks if a string (which is a number) has any leading zeros}
begin
    if (x[1] = '0') and (length(x) <> 1) then
        leadingZeros := true
    else
        leadingZeros := false;
end;

function validateSum(directive : string;var directivesDone : LongInt) : boolean;
{checks if directive of summing is correct. If yes it executes it}
var coma, dot, argument, dummy, argument2, argument3 : LongInt;
    arg1, arg2, arg3 : string;
begin
    validateSum := true;

    coma := pos(',', directive);
    dot := pos('.', directive);
    arg1 := copy(directive, length(const_sumDirective) + 2, coma - pos('(', directive) - 1);
    arg2 := copy(directive, coma + 1, dot - coma - 1);
    arg3 := copy(directive, dot + 2, length(directive) - dot - 2);

    if directivesDone >= const_maxDirectivesDone then
        validateSum := false

    else if dot = 0 then
        validateSum := false

    else if coma = 0 then
        validateSum := false

    else if copy(directive, 1, length(const_sumDirective)) <> const_sumDirective then {correct first few letters}
        validateSum := false

    else if copy(directive, length(const_sumDirective) + 1, 1) <> '(' then {opening bracket}
        validateSum := false

    else if (isNumber(arg1) = false) or (rangeCheck(arg1, const_maxDirectivesDone) = false) or ((leadingZeros(arg1) = true) and (arg1 <> '0')) then {number after bracket, but before coma}
        validateSum := false

    else if (isNumber(arg2) = false) or (rangeCheck(arg2, const_maxInterval) = false) or (leadingZeros(arg2) = true) then {number between , and ..}
        validateSum := false

    else if copy(directive, dot, 2) <> '..' then {two dots in the middle}
        validateSum := false

    else if (isNumber(arg3) = false) or (rangeCheck(arg3, const_maxInterval) = false) or (leadingZeros(arg3) = true) then {number between dots and ) }
        validateSum := false

    else if copy(directive,pos(')', directive),length(directive)) <> ')' then { ) at the end}
        validateSum := false

    else
    begin
        val(arg1, argument, dummy);
        if argument > functionNumber - 1 then
            validateSum := false
        else
        begin
            val(arg2, argument2, dummy);
            val(arg3, argument3, dummy);
            if argument2 > argument3 then
                validateSum := false;
        end;
    end;

    if validateSum = true then
    begin
        writeln(directive, '=', suma(argument, argument2, argument3));
        inc(directivesDone);
    end;
end;

function validateClear(directive : string;var directivesDone : LongInt) : boolean;
{If clear directive is correct it is executed}
var arg : string;
    argument, dummy : LongInt;
begin
    validateClear := true;
    arg := copy(directive, length(const_clearDirective) + 2, length(directive) - length(const_clearDirective) - 2);

    if directivesDone >= const_maxDirectivesDone then
        validateClear := false

    else if copy(directive, 1, length(const_clearDirective)) <> const_clearDirective then {correct beggining}
        validateClear := false

    else if copy(directive, length(const_clearDirective) + 1, 1) <> '(' then {opening bracket}
        validateClear := false

    else if (isNumber(arg) = false) or (rangeCheck(arg, const_maxDirectivesDone) = false) or (leadingZeros(arg) = true) then {number between brackets}
        validateClear := false

    else if copy(directive, length(directive), length(directive)) <> ')' then {closing bracket}
        validateClear := false;

    if validateClear = true then
    begin
        val(arg, argument, dummy);
        if argument >= functionNumber then
            validateClear := false
        else
        begin
            writeln('wezlow: ', czysc(argument));
            inc(directivesDone);
        end;
    end;
end;

function validateAssign(directive : string;var directivesDone : LongInt) : boolean;
{if assignemnt directive is valid it is executed}
var bracket,argument,argument2,dummy : LongInt;
    arg1, arg2 : string;
begin
    validateAssign := true;
    bracket := pos(')', directive);
    arg1 := copy(directive, length(const_functionLetter) + 2, bracket - length(const_functionLetter) - 2);
    arg2 := copy(directive, bracket + 3, length(directive));

    if directivesDone >= const_maxDirectivesDone then
        validateAssign := false

    else if bracket = 0 then
        validateAssign := false

    else if copy(directive, 1, length(const_functionLetter)) <> const_functionLetter then {first letters are name of funcion}
        validateAssign := false

    else if copy(directive, length(const_functionLetter) + 1, 1) <> '(' then {next letter is opening bracket}
        validateAssign := false

    else if (isNumber(arg1) = false) or (rangeCheck(arg1, const_maxInterval) = false) or (leadingZeros(arg1) = true) then {there is a number between brackets}
        validateAssign := false

    else if copy(directive,bracket + 1, 2) <> ':=' then {:= after closing bracket}
        validateAssign := false

    else if (isNumber(arg2) = false) or (rangeCheck(arg2, const_maxInterval) = false) or (leadingZeros(arg2) = true) then {there is a number after :=}
        validateAssign := false; {Too much is copied (length(directive)), but it should not affect the work of program (copy will return only as many sign as there are}

    if validateAssign = true then
    begin
        val(arg1, argument, dummy);
        val(arg2, argument2, dummy);
        writeln('wezlow: ', przypisanie(argument, argument2));
        inc(directivesDone);
    end;
end;

function validate(directive : string;var directivesDone : LongInt) : boolean;
{checks what kind of directive did we receive and sends it to an appropriate validation function}
begin
    validate := false;

    if copy(directive, 1, 1) = copy(const_functionLetter, 1, 1) then {we use copy to get 1st char so that we dont have to worry about empty strings}
        validate := validateAssign(directive, directivesDone)

    else if copy(directive, 1, 1) = copy(const_clearDirective, 1, 1) then
        validate := validateClear(directive, directivesDone)

    else if copy(directive, 1, 1) = copy(const_sumDirective, 1, 1) then
        validate := validateSum(directive, directivesDone);
end;

var directive : string;
    directivesDone : LongInt;
begin
    directivesDone := 0;
    inicjuj();

    while not eof do
    begin
        readln(directive);
        if directivesDone > const_maxDirectivesDone then
            error()
        else if validate(directive, directivesDone) = false then
            error();
    end;
    finish();
end.
